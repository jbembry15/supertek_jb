## How To Get Started With The Workflow:

1. Fork the repository
    * Use the fork button on the right. You may have to click the ... in order to see the options
2.  Clone the repository to your local harddrive.
    *  Click the clone button near the top of the left menu. You may need to click the ... if you don't see it.
    * Copy the link.
    *  Paste the link in a directory on your harddrive that does not have another repository in it.
3.  After the repository is copied, set  your upstream to the main repository
    *  On the bitbucket page, copy the string found in the top left corner of the repository's Overview page.
    * Go to a command line prompt and in the directory with the repository type
    * git remote add upstream string you just copied
    * This will be where you pull the latest develop branch.
4.  Once you get the repository, check out the documentation included in the Business Documents folder

## Code Standards:

### C#
* Use [these](https://www.codeproject.com/Articles/8971/C-Coding-Standards-and-Best-Programming-Practices) C# coding conventions and standards
* use XML comments for all public methods

###  Style
* use external CSS files

### Javascript
* use external script files

### Database
* Entities always plural
* Primary Keys follow this pattern: `UserID`

### Git and Pull Requests
* use branches
* meaningful commit messages
* pull from upstream develop branch to make sure you have all the latest changes
* make sure code compiles before submitting pull request
* create pull requests to merge into develop branch first, instead of master
* resolve all merge conflicts before making pull request
* pull requests should always come from your forked repository



