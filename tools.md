# Tools, Configurations, and Packages

## Microsoft Visual Studio Community 2017 
Version 15.4.3
VisualStudio.15.Release/15.4.3+27004.2008
Microsoft .NET Framework
Version 4.7.02046

Installed Version: Community

## Visual Basic 2017   
00369-60000-00001-AA548
Microsoft Visual Basic 2017

## Visual C# 2017   
00369-60000-00001-AA548
Microsoft Visual C# 2017

## Application Insights Tools for Visual Studio Package   
8.9.00809.2
Application Insights Tools for Visual Studio

## ASP.NET and Web Tools 2017   
15.0.30925.0
ASP.NET and Web Tools 2017

## ASP.NET Core Razor Language Services   
1.0
Provides languages services for ASP.NET Core Razor.

## ASP.NET Web Frameworks and Tools 2017   
5.2.50921.0
For additional information, visit https://www.asp.net/

## Azure App Service Tools 
v3.0.0   15.0.30915.0
Azure App Service Tools v3.0.0

## Azure Data Lake Node   
1.0
This package contains the Data Lake integration nodes for Server Explorer.

## Azure Data Lake Tools for Visual Studio   
2.2.9000.1
Microsoft Azure Data Lake Tools for Visual Studio

## Azure Data Lake Tools for Visual Studio   
2.2.9000.1
Microsoft Azure Data Lake Tools for Visual Studio

## Common Azure Tools  
 1.10
Provides common services for use by Azure Mobile Services and Microsoft Azure Tools.

## JavaScript Language Service  
 2.0
JavaScript Language Service

## Microsoft Azure HDInsight Azure Node  
 2.2.9000.1
HDInsight Node under Azure Node

## Microsoft Azure Hive Query Language Service   
2.2.9000.1
Language service for Hive query

## Microsoft Azure Stream Analytics Language Service   
2.2.9000.1
Language service for Azure Stream Analytics

## Microsoft Azure Stream Analytics Node   
1.0
Azure Stream Analytics Node under Azure Node

## Microsoft Azure Tools   
2.9

## Microsoft Azure Tools for Microsoft Visual Studio 2017 
v2.9.50719.1

## Microsoft Continuous Delivery Tools for Visual Studio   
0.3
Simplifying the configuration of continuous build integration and continuous build delivery from within the Visual Studio IDE.

## Microsoft JVM Debugger   
1.0
Provides support for connecting the Visual Studio debugger to JDWP compatible Java Virtual Machines

## Microsoft MI-Based Debugger   
1.0
Provides support for connecting Visual Studio to MI compatible debuggers

## NuGet Package Manager  
 4.4.0
NuGet Package Manager in Visual Studio. For more information about NuGet, visit http://docs.nuget.org/.

## SQL Server Data Tools   
15.1.61707.200
Microsoft SQL Server Data Tools

## ToolWindowHostedEditor   
1.0
Hosting json editor into a tool window

## TypeScript   
2.3.5.0
TypeScript tools for Visual Studio

## Visual Studio Code Debug Adapter Host Package  
 1.0
Interop layer for hosting Visual Studio Code debug adapters in Visual Studio

### Packages 

## bootstrap
v3.0.0

## jQuery
v1.10.2

## jQuery.Validation 
v1.11.1

## Microsoft.ApplicationInsights
v2.2.0

## Microsoft.ApplicationInsights.Agent.Intercept
v2.0.6

## Microsoft.ApplicationInsights.DependencyCollector
v2.2.0

## Microsoft.ApplicationInsights.PerfCounterCollector
v2.2.0

## Microsoft.ApplicationInsights.Web
v2.2.0

## Microsoft.ApplicationInsights.WindowsServer
v2.2.0 

## Microsoft.ApplicationInsights.WindowsServer.TelemetryChannel
v2.2.0

## Microsoft.AspNet.Mvc
v5.2.3

## Microsoft.AspNet.Razor
v3.2.3

## Microsoft.AspNet.Web.Optimization
v1.1.3

## Microsoft.AspNet.WebPages
v3.2.3

## Microsoft.CodeDom.Providers.DotNetCompilerPlatform
v1.0.7

## Microsoft.jQuery.Unobtrusive.Validation
v3.2.3

## Microsoft.Net.Compilers
v2.1.0

## Microsoft.Web.Infrastructure
v1.0.0

## Modernizr
v2.6.2

## Newtonsoft.Json
v6.0.4

## Respond 
v1.2.0

## WebGrease
v1.5.2





