﻿using System;
using SuperTek_Team_Project.ViewModels;
using SuperTek_Team_Project.Models;
using NUnit.Framework;
using SuperTek_Team_Project.ExternalAPIRequests;
using System.Net;

namespace SuperTekTesting
{
    class LuisTesting
    {
        protected void SetUp()
        {

        }

        [Test]
        public void PluralsightDataModelTest()
        {
            DateTime currentDateTime = DateTime.Now;
            var PluralsightDataTemp = new ProdPluralsightData()
            {
                CourseTitle = "Math",
                DurationInSeconds = 1,
                ReleaseDate = currentDateTime,
                Description = "Basic math",
                AssessmentStatus = "online",
                IsCourseRetired = "No"
            };

            Assert.AreEqual("Math", PluralsightDataTemp.CourseTitle);
            Assert.AreEqual(1, PluralsightDataTemp.DurationInSeconds);
            Assert.AreEqual(currentDateTime, PluralsightDataTemp.ReleaseDate);
            Assert.AreEqual("Basic math", PluralsightDataTemp.Description);
            Assert.AreEqual("online", PluralsightDataTemp.AssessmentStatus);
            Assert.AreEqual("No", PluralsightDataTemp.IsCourseRetired);
        }        

        protected void CleanUp()
        {

        }
    }
}

