﻿using System;
using SuperTek_Team_Project.ViewModels;
using SuperTek_Team_Project.Models;
using NUnit.Framework;
using SuperTek_Team_Project.ExternalAPIRequests;
using System.Net;

namespace SuperTekTesting
{
    class AaronTesting
    {
        [Test]
        public void YouTubeModelTestValidation()
        {
            var YTResult = new YouTube_Models.Snippet()
            {
                title = "c# beginner",
                description = "get started here!",
            };
            Assert.AreEqual("c# beginner", YTResult.title);
            Assert.AreEqual("get started here!", YTResult.description);
        }

        [Test]
        public void APICallToYouTubeReturnsInvalidResponseWhenPassingInvalidWebRequest()
        {
            WebRequest req = WebRequest.Create("https://www.googleapis.com/youtube/v3/search");

            var answer = YouTubeAPI.QueryYT(req);

            var myException = new WebException();

            //Assert.Throws(myException, answer);
        }

        protected void SetUp()
        {

        }

        protected void CleanUp()
        {

        }
    }
}