﻿function beginSearch() {
    var searchQuery = document.getElementById("searchBar").value;
    var level;
    //Set the level the user selected by checking radio buttons
    if (document.getElementById("beg").checked)
        level = "beginning";
    else if (document.getElementById("int").checked)
        level = "intermediate";
    else
        level = "advanced";
    //Safify searchQuery
    var safeQuery = encodeURIComponent(searchQuery);
    //Redirect to method in home controller
    location.href = '/search?query=' + safeQuery + "&level=" + level;
}

function checkEnter(key) {
    if (key.keyCode === 13)
        beginSearch();
}