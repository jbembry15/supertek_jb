﻿/*Skillify DB up script*/
CREATE TABLE SkillifyUsers (
	UserID nvarchar(128) NOT NULL,
	FirstName nvarchar(255),
	LastName nvarchar(255),
	Username nvarchar(255),
	Age int,
	Birthday DATETIME,
	ASPIdentityID nvarchar(128),
	
	PRIMARY KEY (UserID)
);

CREATE TABLE SkillifySearches (
	SearchID int NOT NULL Identity,
	UserID nvarchar(128) NOT NULL,
	Query nvarchar(255) NOT NULL,
	Time DateTime NOT NULL,
	SkillLevel nvarchar(32) NOT NULL,
	Type nvarchar(32) NOT NULL,

	PRIMARY KEY (SearchID),
	FOREIGN KEY (UserID) REFERENCES SkillifyUsers(UserID)
);

CREATE TABLE SkillifyResources (
	ResourceID int NOT NULL IDENTITY,
	SearchID int NOT NULL,
	Title nvarchar(255) NOT NULL,
	Description nvarchar(3000) NOT NULL,
	Photo nvarchar(255) NOT NULL,
	SkillLevel nvarchar(32) NOT NULL,
	Url nvarchar(255) NOT NULL,
	Source nvarchar(100) NOT NULL,
	Type nvarchar(100) NOT NULL,
	IsDone bit,
	SkillifyRating int NOT NULL,
	UserRating int,
	isPaid bit NOT NULL,


	PRIMARY KEY (ResourceID),
	FOREIGN KEY (SearchID) REFERENCES SkillifySearches(SearchID)
);

CREATE TABLE SkillifyCurriculum (
	CurriculumID int NOT NULL IDENTITY,
	SkillifyUserID nvarchar(128) NOT NULL,
	SkillifyResources int NOT NULL,
	DateCreated DateTime NOT NULL,
	DateFinished DateTime,
	IsCreatorFavorite bit NOT NULL,
	IsPublic bit NOT NULL,
	UsersRating int,

	PRIMARY KEY (CurriculumID),
	FOREIGN KEY (SkillifyUserID) REFERENCES SkillifyUsers(UserID),
	FOREIGN KEY (SkillifyResources) REFERENCES SkillifyResources(ResourceID)
);

--The user to save results to if nobody is logged in. -->
INSERT INTO SkillifyUsers VALUES (
	'Not Logged In', 'NULL', 'NULL', 'NULL', -1, -1, 'Not Logged In'
);