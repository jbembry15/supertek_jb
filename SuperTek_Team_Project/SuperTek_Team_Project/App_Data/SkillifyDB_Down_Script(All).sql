﻿--Skillify Created Tables -->
DROP TABLE SkillifyCurriculum;
DROP TABLE SkillifyResources;
DROP TABLE SkillifySearches;
DROP TABLE SkillifyUsers;
--Identity Tables -->
DROP TABLE [dbo].[AspNetUserRoles];
DROP TABLE [dbo].[AspNetRoles];
DROP TABLE [dbo].[AspNetUserClaims];
DROP TABLE [dbo].[AspNetUserLogins];
DROP TABLE [dbo].[AspNetUsers];