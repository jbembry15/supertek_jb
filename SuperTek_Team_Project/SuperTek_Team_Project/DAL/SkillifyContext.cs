namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SkillifyContext : DbContext
    {
        public SkillifyContext()
            : base("name=SkillifyContext")
        {
        }

        public virtual DbSet<SkillifyCurriculum> SkillifyCurriculums { get; set; }
        public virtual DbSet<SkillifyResource> SkillifyResources { get; set; }
        public virtual DbSet<SkillifySearch> SkillifySearches { get; set; }
        public virtual DbSet<SkillifyUser> SkillifyUsers { get; set; }
        public virtual DbSet<ProdPluralsightData> ProdPluralsightDatas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SkillifyResource>()
                .HasMany(e => e.SkillifyCurriculums)
                .WithRequired(e => e.SkillifyResource)
                .HasForeignKey(e => e.SkillifyResources)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SkillifySearch>()
                .HasMany(e => e.SkillifyResources)
                .WithRequired(e => e.SkillifySearch)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SkillifyUser>()
                .HasMany(e => e.SkillifyCurriculums)
                .WithRequired(e => e.SkillifyUser)
                .HasForeignKey(e => e.SkillifyUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SkillifyUser>()
                .HasMany(e => e.SkillifySearches)
                .WithRequired(e => e.SkillifyUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProdPluralsightData>()
               .Property(e => e.CourseId)
               .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.CourseTitle)
                .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.AssessmentStatus)
                .IsUnicode(false);

            modelBuilder.Entity<ProdPluralsightData>()
                .Property(e => e.IsCourseRetired)
                .IsUnicode(false);
        }
    }
}
