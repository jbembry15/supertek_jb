﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using SuperTek_Team_Project.Models;
using SuperTek_Team_Project.ViewModels;

namespace SuperTek_Team_Project.ExternalAPIRequests
{
    public class Pluralsight
    {

        public static IList<SearchResult> Search(string query, string level)
        {
            SkillifyContext db = new SkillifyContext();
            IList<SearchResult> result = new List<SearchResult>();
            try
            {
                IEnumerable<ProdPluralsightData> pluralQuery = db.ProdPluralsightDatas
                             .Where(p => p.CourseTitle.Contains(query)
                                     || p.Description.Contains(query)).ToList();
                // Select courses based on skill level.
                switch (level.ToLower())
                {
                    case ("intermediate"):
                        {
                            pluralQuery = pluralQuery.Where(q => q.CourseTitle.ToLowerInvariant().Contains("intermediate")
                                || q.Description.ToLowerInvariant().Contains("intermediate"));
                            break;
                        }
                    case ("advanced"):
                        {
                            pluralQuery = pluralQuery.Where(q => q.CourseTitle.ToLowerInvariant()
                                .Contains("advanced")
                                || q.Description.ToLowerInvariant().Contains("advanced"));
                            break;
                        }
                    case ("beginner"):
                        {
                            pluralQuery = pluralQuery.Where(q => q.CourseTitle
                                .ToLowerInvariant().Contains("beginner")
                                || q.CourseTitle.ToLowerInvariant().Contains("fundamentals")
                                || q.CourseTitle.ToLowerInvariant().Contains("introduction")
                                || q.Description.ToLowerInvariant().Contains("beginner")
                                || q.Description.ToLowerInvariant().Contains("introduction")
                                || q.Description.ToLowerInvariant().Contains("beginner")
                                );
                            break;
                        }
                    default:
                        {
                            // do more stuff?
                            break;
                        }
                }

                // place the first 10 items into the list.
                int courseCount = pluralQuery.Count();
                int maxResults = 5;
                if (courseCount > 0)
                {
                    // if the course count is less than 10 we want to iterate over
                    // only those items. If the coruse count is greater than or equal to
                    // 10 process only 10.
                    for (int i = 0; i < ((courseCount >= maxResults) ? maxResults : courseCount); i++)
                    {
                        ProdPluralsightData item = pluralQuery.ElementAt(i);
                        SearchResult s = new SearchResult
                        {
                            Title = item.CourseTitle,
                            Description = item.Description,
                            Image = "../Content/pslogo.jpg",
                            Url = "http://www.pluralsight.com/courses/" + item.CourseId,
                            SkillLevel = level,
                            Source = "Pluralsight",
                            Type = "Course"
                        };
                        result.Add(s);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
            return result;
        }
    }
}