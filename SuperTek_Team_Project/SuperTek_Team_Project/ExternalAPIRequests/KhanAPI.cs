﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SuperTek_Team_Project.Models;
using System.Text.RegularExpressions;



namespace SuperTek_Team_Project.ExternalAPIRequests
{
    public class KhanAPI
    {
        public static IList<Models.KhanTopicSearch.Child> KhanAcad(string query, string level)
        {
            //making the string usable by khan academy api if there is spaces within the query
            query = query.TrimEnd('+');
            query = query.Replace("+", "-");

            //Debug.WriteLine(query);

            string source = "https://khanacademy.org/api/v1/topic/" + query;

            //make the web request
            WebRequest request = WebRequest.Create(source);

            //get the response from API
            WebResponse res = request.GetResponse();

            //stream data from server 
            Stream stream = res.GetResponseStream();

            //see what we got here 
            string apiTalk = new StreamReader(stream).ReadToEnd();

            //serialize the string into a JSON object
            var serialize = new JavaScriptSerializer();
            var data = serialize.DeserializeObject(apiTalk);

            //convert JSON into a C# object
            JObject finishedString = JObject.Parse(apiTalk);
            IList<JToken> jsonObject = finishedString["children"].Children().ToList();
            IList<Models.KhanTopicSearch.Child> searchResults = new List<Models.KhanTopicSearch.Child>();

            //class where main important info is 
            //adding each child to the list 
            int maxResults = 5;
            foreach (var result in jsonObject)
            {
                KhanTopicSearch.Child found = result.ToObject<KhanTopicSearch.Child>();
                searchResults.Add(found);
                //Only save the first few found
                if (searchResults.Count >= maxResults)
                    break;
            }


            //closing streams/requests 
            res.Close();
            stream.Close();

            return searchResults;

        }
    }

}
