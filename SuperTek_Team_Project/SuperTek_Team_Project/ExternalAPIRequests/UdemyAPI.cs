﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Udemy_Models;

namespace SuperTek_Team_Project.ExternalAPIRequests
{
    public class UdemyAPI
    {
        public static IList<Result> CourseListSearch(string query, string level)
        {
            //Get secret API keys
            string key = System.Web.Configuration.WebConfigurationManager.AppSettings["UdemyKey"];
            int results = 5;

            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("https://www.udemy.com/api-2.0/courses/?fields[course]=@all,page=1&page_size=" + results + "&search=" + query);
            req.Headers.Add("Authorization", key);
            WebResponse rep = req.GetResponse();
            Stream dataStream = rep.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            //Close streams
            reader.Close();
            rep.Close();

            //Convert from json to C#
            JObject finishedString = JObject.Parse(responseFromServer);

            //IList<JToken> myJsonObject = finishedString["items"].Children().Values("snippet").ToList(); //Old way
            IList<JToken> myJsonObject = finishedString["results"].Children().ToList();

            IList<Result> searchResults = new List<Result>(); //The default class where most of the info is at

            //Fill up the final list to return, filter out what doesn't fit
            foreach (JToken result in myJsonObject)
            {
                Result found = result.ToObject<Result>();
                found.url = "https://www.udemy.com" + found.url; //Formatting the URL correctly
                searchResults.Add(found);
            }

            return searchResults;
        }
    }
}