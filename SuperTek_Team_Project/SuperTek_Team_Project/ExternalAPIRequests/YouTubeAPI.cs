﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace SuperTek_Team_Project.ExternalAPIRequests
{
    public class YouTubeAPI
    {
        public static string QueryYT(WebRequest request)
        {
            WebResponse rep = request.GetResponse();
            Stream dataStream = rep.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string result = reader.ReadToEnd();

            //Close streams
            rep.Close();
            reader.Close();
            dataStream.Close();

            return (result);
        }

        // YouTube regular search
        public static IList<YouTube_Models.Result> Search(string query, string level)
        {
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["YTKey"]; // Get secret API key
            int maxResults = 5; //The # of results to get back
            query += " " + level; //Add the skill level to the query itself

            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=" + maxResults + "&order=relevance&q=" + query + "&key=" + apiKey); //Sends the get to YouTube API

            //Convert from json to C#
            JObject finishedString = JObject.Parse(QueryYT(req));

            IList<JToken> myJsonObject = finishedString["items"].Children().ToList(); //Grab all data from items and put it in json object

            IList<YouTube_Models.Result> searchResults = new List<YouTube_Models.Result>(); //The default class where most of the info is at

            //Add all the results, and filter out ones that shouldn't be there
            foreach (JToken result in myJsonObject)
            {
                YouTube_Models.Result found = result.ToObject<YouTube_Models.Result>();
                //Ignore channels/users
                if (found.id.kind != "youtube#channel")
                {

                    if (found.id.kind == "youtube#video") //For specific video links
                        found.link = "https://www.youtube.com/watch?v=" + found.id.videoId;
                    else if (found.id.kind == "youtube#playlist") //Playlist links
                        found.link = "https://www.youtube.com/playlist?list=" + found.id.playlistId;

                    searchResults.Add(found);
                }
            }

            return searchResults; //Return final list
        }

        public static IList<YouTube_Models.Video_Details_Snippet> VideoSnippet(string id)
        {
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["YTKey"]; // Get secret API key
            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("://www.googleapis.com/youtube/v3/videos?part=" + "snippet" + "&id=" + id + "&key=" + apiKey); //Sends the get to YouTube API

            //Convert from json to C#
            JObject finishedString = JObject.Parse(QueryYT(req));

            IList<JToken> myJsonObject = finishedString["items"].Children().ToList(); //Grab all data from items and put it in json object

            IList<YouTube_Models.Video_Details_Snippet> searchResults = new List<YouTube_Models.Video_Details_Snippet>(); //The default class where most of the info is at

            //Add all the results, and filter out ones that shouldn't be there
            foreach (JToken result in myJsonObject)
            {
                YouTube_Models.Video_Details_Snippet found = result.ToObject<YouTube_Models.Video_Details_Snippet>();

                searchResults.Add(found);
            }

            return searchResults; //Return final list
        }

        public static IList<YouTube_Models.Video_Details_Statistics> VideoStats(string id)
        {
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["YTKey"]; // Get secret API key
            //Send the get request to YouTube
            WebRequest req = WebRequest.Create("://www.googleapis.com/youtube/v3/videos?part=" + "statistics" + "&id=" + id + "&key=" + apiKey); //Sends the get to YouTube API

            //Convert from json to C#
            JObject finishedString = JObject.Parse(QueryYT(req));

            IList<JToken> myJsonObject = finishedString["items"].Children().ToList(); //Grab all data from items and put it in json object

            IList<YouTube_Models.Video_Details_Statistics> searchResults = new List<YouTube_Models.Video_Details_Statistics>(); //The default class where most of the info is at

            //Add all the results, and filter out ones that shouldn't be there
            foreach (JToken result in myJsonObject)
            {
                YouTube_Models.Video_Details_Statistics found = result.ToObject<YouTube_Models.Video_Details_Statistics>();

                searchResults.Add(found);
            }

            return searchResults; //Return final list
        }
    }
}