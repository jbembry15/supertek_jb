﻿using System;
using System.Collections.Generic;

namespace YouTube_Models
{
    public class PageInfo
    {
        public int totalResults { get; set; }
        public int resultsPerPage { get; set; }
    }

    public class Id
    {
        public string kind { get; set; }
        public string videoId { get; set; }
        public string channelId { get; set; }
        public string playlistId { get; set; }
    }

    public class Default
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Medium
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class High
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Thumbnails
    {
        public Default @default { get; set; }
        public Medium medium { get; set; }
        public High high { get; set; }
    }

    public class Snippet
    {
        public DateTime publishedAt { get; set; }
        public string channelId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public Thumbnails thumbnails { get; set; }
        public string channelTitle { get; set; }
        public string liveBroadcastContent { get; set; }
    }

    public class Result
    {
        public string kind { get; set; } //The type this result is, such as list response for a normal search
        public string etag { get; set; } //Not quite sure
        public Id id { get; set; } //Contains info about the specific result, like id of video, channel id
        public Snippet snippet { get; set; } //All the information about the video returned
        public int STRating { get; set; } //SUPERTEK's algorithmic rating for this specific video
        public string link { get; set; } //The formatted, finished, clickable link to then be used on SearchResults.cshtml
    }

    public class RootObject
    {
        public string kind { get; set; }
        public string etag { get; set; }
        public string nextPageToken { get; set; }
        public string regionCode { get; set; }
        public PageInfo pageInfo { get; set; }
        public List<Result> items { get; set; }
    }
}