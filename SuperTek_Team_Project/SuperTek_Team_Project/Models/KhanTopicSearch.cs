﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperTek_Team_Project.Models
{
    public class KhanTopicSearch
    {
        public class ChildData
        {
            public string kind { get; set; }
            public string id { get; set; }
        }

        public class Child
        {
            public string description { get; set; }
            public string internal_id { get; set; }
            public string node_slug { get; set; }
            public string icon_large { get; set; }
            public string key { get; set; }
            public string translated_title { get; set; }
            public string id { get; set; }
            public string icon { get; set; }
            public string kind { get; set; }
            public bool hide { get; set; }
            public string relative_url { get; set; }
            public string title { get; set; }
            public string url { get; set; }
            public string edit_slug { get; set; }
            public string translated_description { get; set; }
            public string translated_standalone_title { get; set; }
            public string standalone_title { get; set; }
        }

        public class TranslatedCuration
        {
            public List<string> blacklist { get; set; }
            public List<object> modules { get; set; }
        }

        public class Curation
        {
            public List<string> blacklist { get; set; }
        }

        public class RootObject
        {
            public string icon_src { get; set; }
            public string domain_slug { get; set; }
            public string relative_url { get; set; }
            public bool has_topic_unit_test { get; set; }
            public DateTime creation_date { get; set; }
            public string sha { get; set; }
            public string web_url { get; set; }
            public string ka_url { get; set; }
            public string translated_custom_description_tag { get; set; }
            public string translated_title { get; set; }
            public bool has_user_authored_content_types { get; set; }
            public string standalone_title { get; set; }
            public string author_key { get; set; }
            public string gplus_url { get; set; }
            public string id { get; set; }
            public bool importable { get; set; }
            public List<object> concept_tags { get; set; }
            public string old_key_name { get; set; }
            public bool hide { get; set; }
            public string node_slug { get; set; }
            public bool do_not_publish { get; set; }
            public List<ChildData> child_data { get; set; }
            public string source_language { get; set; }
            public List<object> user_authored_content_types_info { get; set; }
            public List<Child> children { get; set; }
            public string twitter_url { get; set; }
            public string translated_description { get; set; }
            public bool listed { get; set; }
            public object endorsement { get; set; }
            public List<object> user_authored_content_types { get; set; }
            public DateTime deleted_mod_time { get; set; }
            public string logo_image_url { get; set; }
            public bool in_knowledge_map { get; set; }
            public bool enable_fpm_mastery { get; set; }
            public string translated_standalone_title { get; set; }
            public string description { get; set; }
            public int x_pos { get; set; }
            public string custom_title_tag { get; set; }
            public bool deleted { get; set; }
            public List<string> listed_locales { get; set; }
            public string facebook_url { get; set; }
            public DateTime backup_timestamp { get; set; }
            public string render_type { get; set; }
            public string background_image_url { get; set; }
            public string background_image_caption { get; set; }
            public bool sponsored { get; set; }
            public bool lower_toc { get; set; }
            public bool has_peer_reviewed_projects { get; set; }
            public string topic_page_url { get; set; }
            public string extended_slug { get; set; }
            public string slug { get; set; }
            public List<string> tags { get; set; }
            public TranslatedCuration translated_curation { get; set; }
            public string kind { get; set; }
            public bool show_topics_module { get; set; }
            public string custom_description_tag { get; set; }
            public DateTime date_modified { get; set; }
            public bool in_topic_browser { get; set; }
            public Curation curation { get; set; }
            public string title { get; set; }
            public string translated_custom_title_tag { get; set; }
            public object imported_from_sha { get; set; }
            public string branding_image_url { get; set; }
            public bool has_topic_quiz { get; set; }
            public int y_pos { get; set; }
            public string current_revision_key { get; set; }
            public string content_id { get; set; }
            public string content_kind { get; set; }
            public string icon { get; set; }
            public string curriculum_key { get; set; }
        }
    }
}