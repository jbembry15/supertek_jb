namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SkillifyCurriculum")]
    public partial class SkillifyCurriculum
    {
        [Key]
        public int CurriculumID { get; set; }

        [Required]
        [StringLength(128)]
        public string SkillifyUserID { get; set; }

        public int SkillifyResources { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateFinished { get; set; }

        public bool IsCreatorFavorite { get; set; }

        public bool IsPublic { get; set; }

        public int? UsersRating { get; set; }

        public virtual SkillifyUser SkillifyUser { get; set; }

        public virtual SkillifyResource SkillifyResource { get; set; }
    }
}
