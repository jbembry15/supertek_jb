namespace SuperTek_Team_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SkillifyResource
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SkillifyResource()
        {
            SkillifyCurriculums = new HashSet<SkillifyCurriculum>();
        }

        [Key]
        public int ResourceID { get; set; }

        public int SearchID { get; set; }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        [Required]
        [StringLength(3000)]
        public string Description { get; set; }

        [Required]
        [StringLength(255)]
        public string Photo { get; set; }

        [Required]
        [StringLength(32)]
        public string SkillLevel { get; set; }

        [Required]
        [StringLength(255)]
        public string Url { get; set; }

        [Required]
        [StringLength(100)]
        public string Source { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        public bool? IsDone { get; set; }

        public int SkillifyRating { get; set; }

        public int? UserRating { get; set; }

        public bool isPaid { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SkillifyCurriculum> SkillifyCurriculums { get; set; }

        public virtual SkillifySearch SkillifySearch { get; set; }
    }
}
