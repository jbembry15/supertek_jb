﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperTek_Team_Project.ViewModels
{
    public class SearchResult
    {
        public string Title { get; set; } //The title of the video/playlist
        public string Description { get; set; } //Description of video/playlist
        public string Image { get; set; } //The final image to display
        public string Url { get; set; } //Finished, clickable URL
        public string SkillLevel { get; set; } //skill level associated with video/playlist
        public string Type { get; set; } //What kind of resource it is, such as Video, Exercise, Playlist, or Course
        public string Source { get; set; } //Where the video comes from, YouTube, Udemy, etc..
    }
}