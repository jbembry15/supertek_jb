﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SuperTek_Team_Project.ViewModels;
using SuperTek_Team_Project.Models;
using System.Net;
using System.Data.Entity;
using System.Data;
using Microsoft.AspNet.Identity;

namespace SuperTek_Team_Project.Controllers
{
    public class UserPortalController : Controller
    {
        private SkillifyContext db = new SkillifyContext();

        // GET: UserPortal/SearchHistory
        public ActionResult SearchHistory()
        {
            //get logged in user's ID 
            var aspID = User.Identity.GetUserId();

            //initialize SearchHistoryModel list
            List<SearchHistoryModel> history = new List<SearchHistoryModel>();
            
            //grab searches conducted by the user in descending order
            var query =
                   (from s in db.SkillifySearches
                    join r in db.SkillifyResources
                    on s.SearchID equals r.SearchID
                    where s.SkillifyUser.UserID == aspID
                    orderby s.Time descending

                    select new
                    { query = s.Query, date = s.Time, description= r.Description,
                        url = r.Url, title = r.Title}).ToList();

            //add each item found into the list 
             foreach (var item in query)
            {
                SearchHistoryModel sh = new SearchHistoryModel
                {
                    search = item.query,
                    date = item.date,
                    description = item.description,
                    title = item.title,
                    url = item.url
                };
                history.Add(sh);

            }
            return View(history);
}
        }
    }
