﻿using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using SuperTek_Team_Project.Models;
using SuperTek_Team_Project.ViewModels;
using System;
using Microsoft.AspNet.Identity;
using SuperTek_Team_Project.ExternalAPIRequests;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace SuperTek_Team_Project.Controllers
{
    public class HomeController : Controller
    {

        SkillifyContext db = new SkillifyContext();

        [RequireHttps]
        public ActionResult Index()
        {
              return View();
        }

        [RequireHttps]
        public ActionResult SearchResults(string query = "", string level = "")
        {
            //Grab the aspects of the query string that is needed
            query = Request.QueryString["query"];
            level = Request.QueryString["level"];
           
            //Console.WriteLine(level);
            //Encode to make it safe to search
            query = Server.UrlEncode(query);

            //Check if it's a valid search
            if (query == null)
                return View("~/Views/Home/Index.cshtml");
            //Then run the search if it is
            else
            {
                //Convert the serach results into one file to return
                List<SearchResult> finalList = new List<SearchResult>();
                //Create controllers and gather the data
                try
                {
                    //call udemy api class 
                    var udemyData = UdemyAPI.CourseListSearch(query, level);
                    foreach (var item in udemyData)
                    {
                        //assign a skill level to a Search result item 
                        //if a description has any of the following words,assign it intermediate,
                        //advanced, or as a default, beginning (same for all the website apis) 
                        string skillLevel;
                        if (item.published_title.ToUpperInvariant().Contains("INTERMEDIATE"))
                        {
                            skillLevel = "intermediate";
                        }
                        else if (item.published_title.ToUpperInvariant().Contains("ADVANCED"))
                        {
                            skillLevel = "advanced";
                        }
                        else
                            skillLevel = "beginning";

                        //save the search result based on the information found by the API call
                        SearchResult temp = new SearchResult
                        {
                            Title = item.title,
                            Description = item.description.Substring(0, 250),
                            Image = item.image_480x270,
                            Url = item.url,
                            SkillLevel = skillLevel,
                            Source = "Udemy",
                            Type = "Course" //May get more specific, pulling out exercies later
                        };
                        //if the skill level equals the level from the query string, add to the list 
                        //same for all API calls
                        if (temp.SkillLevel == level)
                        {
                            finalList.Add(temp);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                try
                {
                    //call Youtube API class 
                    var youTubeData = YouTubeAPI.Search(query, level);
                    foreach (var item in youTubeData)
                    {
                        string skillLevel;
                        if (item.snippet.description.ToUpperInvariant().Contains("INTERMEDIATE"))
                        {
                            skillLevel = "intermediate";
                        }
                        else if (item.snippet.description.ToUpperInvariant().Contains("ADVANCED"))
                        {
                            skillLevel = "advanced";
                        }
                        else
                            skillLevel = "beginning";

                        SearchResult temp = new SearchResult
                        {
                            Title = item.snippet.title,
                            Description = item.snippet.description,
                            Image = item.snippet.thumbnails.high.url,
                            Url = item.link,
                            SkillLevel = skillLevel,
                            Source = "YouTube",
                            Type ="NULL FOR NOW"
                        };

                   
                        if (temp.SkillLevel == level)
                        {
                            finalList.Add(temp);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                try
                {

                    //call Khan Academy API class
                    var khanData = KhanAPI.KhanAcad(query, level);
                
                    foreach (var item in khanData)
                    {
                        string skillLevel;
                        if (item.description.ToUpperInvariant().Contains("INTERMEDIATE"))
                        {
                            skillLevel = "intermediate";
                        }
                        else if (item.description.ToUpperInvariant().Contains("ADVANCED"))
                        {
                            skillLevel = "advanced";
                        }
                        else
                            skillLevel = "beginning";


                        SearchResult temp = new SearchResult
                        {
                            Title = item.title,
                            Description = item.description,
                            Image = item.icon_large,
                            Url = item.url,
                            SkillLevel = skillLevel,
                            Source = "Khan Academy",
                            Type = "Course"
                        };

                        if(temp.SkillLevel == level)
                        {
                            finalList.Add(temp);
                        }
                        
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                // Get search results from Plurasight
                try
                {
                    IList<SearchResult> pluralResults = Pluralsight.Search(query, level);
                    foreach(SearchResult s in pluralResults)
                    {
                        finalList.Add(s);
                    }
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
                //save searches and results if the user is logged in
                if (User.Identity.IsAuthenticated)
                {
                    SkillifySearch search = new SkillifySearch();
                    search.Query = query;
                    search.Time = DateTime.Now;
                    //setting the level, and if it is null, save it as beginning
                    if (level != null)
                    {
                        search.SkillLevel = level;
                    }
                    else
                        search.SkillLevel = "beginning";
                    search.Type = "skill";

                    var aspID = User.Identity.GetUserId();
                    search.UserID = db.SkillifyUsers.Find(aspID).UserID;
                    //saving searches to database
                    db.SkillifySearches.Add(search);
                    db.SaveChanges();

                    foreach (SearchResult item in finalList)
                    {       
                        //initialize result object and assign it to unknown if the attribute is null
                        //else save it as the attribute that is in the item 
                        SkillifyResource result = new SkillifyResource();
                        if (item.Title == null)
                            result.Title = "Unknown";
                        else
                            result.Title = item.Title;
                        if(item.Description == null || item.Description == "")
                            result.Description = "Unknown";
                        else
                            result.Description = (item.Description.Length <= 252) ? item.Description : item.Description.Substring(0, 251) + "...";  //if description is longer than 252 chars, cut string at 251 and add ...
                        if (item.Image == null)
                            result.Photo = "Unknown";
                        else
                            result.Photo = item.Image;
                        if (item.Url == null)
                            result.Url = "Unknown";
                        else
                            result.Url = item.Url;
                        if (item.SkillLevel == null)
                            result.SkillLevel = "Unknown";
                        else
                            result.SkillLevel = item.SkillLevel;

                        result.Source = item.Source;
                        result.Type = item.Type;

                        result.SearchID = search.SearchID;
                        //saving results to database
                        db.SkillifyResources.Add(result);
                        db.SaveChanges();
                    }
                }
                else //Still save the query
                {
                    SkillifySearch search = new SkillifySearch();
                    search.Query = query;
                    search.Time = DateTime.Now;
                    search.SkillLevel = level;
                    search.Type = "skill";

                    //default if the user is not logged in 
                    search.UserID = "Not Logged In";

                    db.SkillifySearches.Add(search);
                    db.SaveChanges();
                }

                return View(finalList);
            }
          
        }

        /// <summary>
        /// Updates the Pluralsight course database table with current courses from
        /// a csv file downloaded from pluralsight url: https://api.pluralsight.com/api-v0.9/courses
        /// </summary>
        //[Authorize(Roles = "Admin")]
        [RequireHttps]
        public ActionResult PluralsightUpdater()
        {
            WebRequest psRequest = HttpWebRequest.Create("https://api.pluralsight.com/api-v0.9/courses");
            List<ProdPluralsightData> theList = new List<ProdPluralsightData>();
            int courseCount = 0;
            using (WebResponse csv = psRequest.GetResponse())
            {
                Stream theStream = csv.GetResponseStream();
                StreamReader theStreamReader = new StreamReader(theStream);
                             
                string line = null;

                while ( (line = theStreamReader.ReadLine()) != null )
                {
                    string[] tempArray = new Regex(","+"(?=(?:[^\"]*\"[^\"]*\")*(?!([^\"]*\")))").Split(line);
                    if (tempArray.Length == 7)
                    {
                        if((tempArray[0].Equals("CourseID", StringComparison.OrdinalIgnoreCase) == false) 
                            && tempArray[6].Equals("yes", StringComparison.OrdinalIgnoreCase) == false)
                        {
                            ProdPluralsightData theCourse = new ProdPluralsightData
                            {
                                CourseId = tempArray[0],
                                CourseTitle = tempArray[1],
                                DurationInSeconds = Convert.ToInt32(tempArray[2]),
                                ReleaseDate = Convert.ToDateTime(tempArray[3]),
                                //if the course description is longer than the varchar size, truncate the string and add ..., else add the line as it is
                                Description = (tempArray[4].Length > 950) ? (tempArray[4].Substring(0, 945) + "...") : tempArray[4],
                                AssessmentStatus = tempArray[5],
                                IsCourseRetired = tempArray[6]
                            };
                            theList.Add(theCourse);
                            courseCount++;
                        }

                    }
                    else
                    {
                        ViewBag.Message = "Error with one or more lines in CSV file!";
                    }
                }

            }
            db.Database.ExecuteSqlCommand("TRUNCATE TABLE ProdPluralsightData");
            foreach(ProdPluralsightData item in theList)
            {
                try
                {
                    db.ProdPluralsightDatas.Add(item);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }

            ViewBag.Message = "There are now " + Convert.ToString(courseCount) + " Pluralsight courses available.";
            return View(theList);
        }
    }
}